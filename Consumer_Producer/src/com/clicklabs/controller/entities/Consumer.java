package com.clicklabs.controller.entities;

import java.util.Vector;

public class Consumer implements Runnable {

	private final Vector<?> sharedQueue;
	

	public Consumer(Vector<?> sharedQueue, int size) {
		this.sharedQueue = sharedQueue;
	}

	@Override
	public void run() {
		while (true) {
			try {
				System.out.println("Consumed: " + consume());
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				System.out.println(e);
			}

		}
	}

	private int consume() throws InterruptedException {
		while (sharedQueue.isEmpty()) {
			synchronized (sharedQueue) {
				System.out.println(" \t size: \t" + sharedQueue.size());

				sharedQueue.wait();
			}
		}

		synchronized (sharedQueue) {
			sharedQueue.notifyAll();
			return (Integer) sharedQueue.remove(0);
		}
	}
	

	

}