package com.clicklabs.controller.entities;

import java.util.Vector;

public class Producer implements Runnable {

	private final Vector<Integer> sharedQueue;
	private final int SIZE;
	public boolean flag = false;

	public Producer(Vector<Integer> sharedQueue, int size) {
		this.sharedQueue = sharedQueue;
		this.SIZE = 20;
	}

	@Override
	public void run() {
		int item = 0;
		try {
			while (flag == false) {
				System.out.println("Produced: " + item);
				produce(item);
				item++;
			}
		} catch (InterruptedException ex) {
			System.out.println(ex);
		}

	}

	private void produce(int item) throws InterruptedException {
		while (sharedQueue.size() == SIZE) {
			synchronized (sharedQueue) {
				System.out.println("\tsize: \t" + sharedQueue.size());
				sharedQueue.wait();
			}
		}
		synchronized (sharedQueue) {
			sharedQueue.add(item);
			sharedQueue.notifyAll();
		}
	}

}