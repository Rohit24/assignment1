package com.clicklabs.controller;

import java.util.Scanner;
import java.util.Vector;

import com.clicklabs.controller.entities.*;

public class Controller {
	public static Scanner scanner = new Scanner(System.in);
	public static Vector<Integer> sharedQueue = new Vector<Integer>();

	public static void main(String[] args) {
		int size = 20;
		Producer prod = new Producer(sharedQueue, size);
		Consumer cons = new Consumer(sharedQueue, size);
		Thread prodThread = new Thread(prod, "Producer");
		Thread consThread = new Thread(cons, "Consumer");
		prodThread.start();
		consThread.start();
		while (true) {
			
			if (scanner.next() != null) {

				consThread.stop();
				prodThread.stop();
				break;
			}
		}

	}
}